from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.

def inscriptionPage(request):
    context={}
    return render(request,'compte/inscription.html',context)

def accesPage(request):
    context={}
    return render(request,'compte/acces.html',context)
    
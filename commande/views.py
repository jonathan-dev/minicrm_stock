from django.http import  HttpResponseRedirect 
from django.http import HttpResponse
from django.shortcuts import render
import pkg_resources
from .forms import CommandeForm
from .models import Commande
from .import models
from commande import forms

# Create your views here.
context = {}
context = {'form':forms}

def list_commande(request):
    return render(request,'commande/list_commande.html')

def ajouter_commande(request):
    form=CommandeForm()
    if request.method =='POST':
        form=CommandeForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/')
        #context={'form':form}
    return render(request,'commande/ajouter_commande.html',{'form':form})

def modifier_commande(request,pk):
    commande = Commande.objects.get(id=pk)
    form=CommandeForm(instance=commande)
    if request.method =='POST':
        form=CommandeForm(request.POST,instance=commande)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/')
        #context={'form':form}
    return render(request,'commande/ajouter_commande.html',{'form':form})

def supprimer_commande(request,pk):
    commande=Commande.objects.get(id=pk)
    if request.method=='POST':
       commande.delete()
       return HttpResponseRedirect('/')
    context1={'item':commande}
    return render(request,'commande/supprimer_commande.html',context1)